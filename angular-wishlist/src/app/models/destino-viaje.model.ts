export class DestinoViaje {
    private selected :boolean;   
    servicios :string[]; 
    constructor(public nombre :string,public imageUrl:string) {     
        this.servicios = ['pileta','desayuno'];   
    }

    isSelected() :boolean{
        return this.selected;
    }
    setSelected(value:boolean) :void {
        this.selected = value;
    }
}