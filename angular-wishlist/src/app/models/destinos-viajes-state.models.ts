import { DestinoViaje } from './destino-viaje.model';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



export interface DestinosViajesSate{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const InitializeDestinoViajeState = function(){
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

export enum DestinoViajeActionType{
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELGIDO_FAVORITO = '[Destinos Viajes] Favorito'
}

export class NuevoDestinoAction implements Action{
    type = DestinoViajeActionType.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action{
    type = DestinoViajeActionType.ELGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){}
}

export type DestinoViajeAction = NuevoDestinoAction | ElegidoFavoritoAction;

export function ReducerDestinosViajes(state: DestinosViajesSate, action: DestinoViajeAction)
:DestinosViajesSate{
    switch (action.type) {
        case DestinoViajeActionType.NUEVO_DESTINO:
            return{
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        case DestinoViajeActionType.ELGIDO_FAVORITO:
            {
                state.items.forEach(x => x.setSelected(false));
                let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
                fav.setSelected(true);
                return{
                    ...state,
                    favorito: fav
                };
            }      
    }
    return state;
}

@Injectable()
export class DestinosViajesEffects{
    @Effect()
    NuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajeActionType.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private actions$: Actions){}   
}

