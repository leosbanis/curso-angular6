import { Component, OnInit, Output, EventEmitter, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Autor } from 'src/app/models/autor.model';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';

@Component({
  selector: 'app-form-autor',
  templateUrl: './form-autor.component.html',
  styleUrls: ['./form-autor.component.css']
})
export class FormAutorComponent implements OnInit {
  @Output() onItemAdd: EventEmitter<Autor>;
  fg: FormGroup;
  paisesBusqueda: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.fg = fb.group({
      nombre: [''],
      apellido: [''],
      pais: ['']
    });
    this.onItemAdd = new EventEmitter<Autor>();
   }

  ngOnInit() {
    let element = <HTMLInputElement> document.getElementById('pais');
    fromEvent(element, 'input')
      .pipe(
        map((e: KeyboardEvent)=>(e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string)=>ajax(this.config.apiEndpoint + '/ciudades?q=' +text))
      ).subscribe(ajaxResponse => this.paisesBusqueda = ajaxResponse.response)
  }

  guardar(nombre: string, apellido: string, pais: string){
    let a = new Autor(nombre,apellido,pais);
    this.onItemAdd.emit(a);
    return false;
  }

}
