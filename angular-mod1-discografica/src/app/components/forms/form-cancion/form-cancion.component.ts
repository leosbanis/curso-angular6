import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { Cancion } from '../../../models/cancion';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';


@Component({
  selector: 'app-form-cancion',
  templateUrl: './form-cancion.component.html',
  styleUrls: ['./form-cancion.component.css']
})
export class FormCancionComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Cancion>;
  @Input() generos;
  fg:FormGroup;
  longitud = 3;
  constructor(fb: FormBuilder) {
    this.fg = fb.group({
      titulo: ['',Validators.compose([
        Validators.required
      ])],
      genero: [''],
      autor: ['',Validators.compose([
        this.nombreAutorValidator(this.longitud)
      ])]
    });
    this.onItemAdded = new EventEmitter();
   }

  ngOnInit() {

  }

  guardar(titulo:string, genero:string, autor: string):boolean{
    let cancion = new Cancion(titulo, genero, autor);
    this.onItemAdded.emit(cancion);
    return false;
  }

  nombreAutorValidator(longitud :number) :ValidatorFn {
    return (control : FormControl):{[s :string] :boolean}=>{
      const l = control.value.toString().trim().length;
      const valor: string = control.value.toString();
      if(l>0&&l<longitud){
          return {longitudInvalida :true};
        }       
      return null;
    }
  }
}
