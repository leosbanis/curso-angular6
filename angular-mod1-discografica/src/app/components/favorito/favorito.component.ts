import { Component, OnInit, Output, EventEmitter, HostBinding, Input } from '@angular/core';
import { Cancion } from '../../models/cancion';

@Component({
  selector: 'app-favorito',
  templateUrl: './favorito.component.html',
  styleUrls: ['./favorito.component.css']
})
export class FavoritoComponent implements OnInit {
  flag: boolean = false; 
  @Output() notificarFavorito :EventEmitter<Cancion>;
  constructor() { 
    this.notificarFavorito = new EventEmitter();
  }

  favorito(){
    this.notificarFavorito.emit(); 
    this.flag = !this.flag;   
  }

  ngOnInit() {
  }

}
