import { OnInit, Component } from '@angular/core';
import { CancionesApiClient } from 'src/app/models/cancion-api-client.model';
import { AlbumComponent } from '../../album/album.component';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { CancionDelMesApi } from 'src/app/models/cancion-del-mes-api.model';

@Component({
    selector: 'app-cancion-mes',
    templateUrl: './cancion-mes.html',
    providers:[{provide: CancionesApiClient, useClass: CancionDelMesApi}]
  })

  export class CancionDelMesComponent extends AlbumComponent implements OnInit{   
    cancionMes;
      constructor(cancionApiClient: CancionesApiClient, store: Store<AppState>){
          super(store,cancionApiClient);
            this.cancionMes = cancionApiClient.getCancionDelMes();
      }
      ngOnInit() {  
    }
  }