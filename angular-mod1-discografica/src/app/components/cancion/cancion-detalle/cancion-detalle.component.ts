import { Component, OnInit} from '@angular/core';
import { CancionesApiClient } from 'src/app/models/cancion-api-client.model';
import { ActivatedRoute } from '@angular/router';
import { Cancion } from 'src/app/models/cancion';


@Component({
  selector: 'app-cancion-detalle',
  templateUrl: './cancion-detalle.component.html',
  styleUrls: ['./cancion-detalle.component.css'],
  
})

export class CancionDetalleComponent implements OnInit {
  cancion: Cancion;
  constructor(private route: ActivatedRoute, private cancionApiClient: CancionesApiClient) { 
      
   }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.cancion = this.cancionApiClient.getById(id);
  }

}
