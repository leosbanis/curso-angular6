import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Cancion} from '../../models/cancion';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { CancionesApiClient } from '../../models/cancion-api-client.model';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit { 
  @Output() onCancionAdded: EventEmitter<Cancion>;
  @Output() generos :string[];
  favoritas;
  album;
  votosPositivos;
  votosNegativos;
  constructor(private store: Store<AppState>, private cancionApiCliente: CancionesApiClient) {
    this.onCancionAdded = new EventEmitter();    
    this.generos = [
      'Dance','Pop','Rock', 'Alternativa'
    ];
    this.store.select(state => state.cancion.favoritos).subscribe(fav => {
      this.favoritas = fav;
    });     
    this.store.select(state => state.cancion.items).subscribe(items => {
      this.album = items;     
    });
    this.store.select(state => state.votos.votosPositivos).subscribe(posit => {
      this.votosPositivos = posit;
    });
    this.store.select(state => state.votos.votosNegativos).subscribe(negat => {
      this.votosNegativos = negat;
    });
   }

  guardarCancion(cancion: Cancion){
    this.cancionApiCliente.add(cancion);
    this.onCancionAdded.emit(cancion);
  }
  marcadoFavorito(cancion :Cancion){
    this.cancionApiCliente.elegir(cancion);
  }
  votoMas(cancion: Cancion){
    this.cancionApiCliente.votoMas(cancion);
  }
  votoMenos(cancion: Cancion){
    this.cancionApiCliente.votoMenos(cancion)
  }

  ngOnInit() {
  }

}
