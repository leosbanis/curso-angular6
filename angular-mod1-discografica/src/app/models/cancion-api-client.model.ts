import { Injectable, forwardRef, Inject } from "@angular/core";
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Cancion } from './cancion';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { NuevaCancionAction, NuevaFavoritoAction, ElegidoMasRecienteAction, VotoMasAction, VotoMenosAction } from './canciones-state.model';

@Injectable()
export class CancionesApiClient{
    canciones: Cancion[] = [];
    constructor(private store: Store<AppState>, private http: HttpClient,
        @Inject(forwardRef(()=>APP_CONFIG))private config: AppConfig){
        this.store
        .select(state => state.cancion)
        .subscribe((data)=>{
            //console.log("cancion sub store");
            console.log(data);
            this.canciones = data.items;
        });
        this.store.subscribe((data) => {
            //console.log("all store");
           console.log(data);
        })
    }

    add(cancion :Cancion){
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', 
        cancion,{headers: headers});
        this.http.request(req).subscribe((data: HttpResponse<{}>)=>{
            if(data.status === 200){
                this.store.dispatch(new NuevaCancionAction(cancion));
                const myDb = db;
                myDb.canciones.add(cancion);
                myDb.canciones.toArray().then(canciones => console.log(canciones));
            }
        });             
    }    
    elegir(cancion :Cancion){
        this.store.dispatch(new NuevaFavoritoAction(cancion));
    }
    reciente(cancion: Cancion){
        this.store.dispatch(new ElegidoMasRecienteAction(cancion));
    }
    votoMas(cancion: Cancion){
        this.store.dispatch(new VotoMasAction(cancion));
    } 
    votoMenos(cancion: Cancion){
        this.store.dispatch(new VotoMenosAction(cancion));
    }
    getById(id: string):Cancion{
        return this.canciones.filter(function(c){return c.id===id;})[0];    
    }
    descargar(cancion: Cancion){
        
    }   
    getCancionDelMes(){}
}