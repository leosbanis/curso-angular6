import {v4 as uuid} from 'uuid';

export class Cancion{
    titulo :string;
    autor :string;
    genero :string;
    favorita :boolean;
    mas_reciente: boolean;
    votos: number;
    id = uuid();
    

    constructor(titulo :string, genero :string, autor :string){
        this.titulo = titulo;        
        this.genero = genero;
        this.autor = autor;
        this.favorita = false;
        this.votos = 0;
        this.mas_reciente = true;
    }

    setFavorita(value: boolean){
        this.favorita = value;
    }

    isFavorita() :boolean{
        return this.favorita;
    }

    setMasReciente(value: boolean){
        this.mas_reciente = value;
    }

    isMasReciente():boolean{
        return this.mas_reciente;
    }
    getVotos(){return this.votos;}
    getId(){return this.id;}

}