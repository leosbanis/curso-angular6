import { initializaCancionState, InitMyDataAction, CancionState, 
    ReducerCancion, NuevaCancionAction, NuevaFavoritoAction, VotoMasAction, VotoMenosAction } from "./canciones-state.model";
import { Cancion } from './cancion';

describe('ReducerCancion', ()=>{
    it('should reduce init data', ()=>{
        const prevState = initializaCancionState();
        const action: InitMyDataAction = new InitMyDataAction([
            new Cancion('ee','ee','ee'),new Cancion('ii','ii','ii')]);
        const newState: CancionState = ReducerCancion(prevState,action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].titulo).toEqual('ee');
    });
    it('should reduce new item added', ()=>{
        const prevState = initializaCancionState();
        const action: NuevaCancionAction = new NuevaCancionAction(new Cancion('ee','ee','ee'));
        const newState: CancionState = ReducerCancion(prevState,action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].titulo).toEqual('ee');
    });
    it('should reduce new favorite added', ()=>{
        const prevState = initializaCancionState();
        const action: NuevaFavoritoAction = new NuevaFavoritoAction(new Cancion('ee','ee','ee'));
        const newState: CancionState = ReducerCancion(prevState,action);
        expect(newState.favoritos.length).toEqual(1);
        expect(newState.favoritos[0].titulo).toEqual('ee');
    });
    it('should reduce remove favorite', ()=>{
        let favorita = new Cancion('ee','ee','ee');        
        const action1: NuevaFavoritoAction = new NuevaFavoritoAction(favorita);
        const prevState: CancionState = ReducerCancion(initializaCancionState(),action1);
        const action2: NuevaFavoritoAction = new NuevaFavoritoAction(favorita);
        const newState: CancionState = ReducerCancion(prevState,action2);
        expect(newState.favoritos.length).toEqual(0);
    });
    it('should reduce more recent', ()=>{
        let cancion = new Cancion('ee','ee','ee');  
        let reciente = new Cancion('aa','aa','aa'); 
        const action1: NuevaCancionAction = new NuevaCancionAction(cancion);
        const prevState: CancionState = ReducerCancion(initializaCancionState(),action1);
        const action2: NuevaCancionAction = new NuevaCancionAction(reciente);
        const newState: CancionState = ReducerCancion(prevState,action2);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[1].mas_reciente).toEqual(true);
    });
    it('should reduce vote up', ()=>{
        let cancion = new Cancion('ee','ee','ee');          
        const action1: NuevaCancionAction = new NuevaCancionAction(cancion);
        const prevState: CancionState = ReducerCancion(initializaCancionState(),action1);
        const action2: VotoMasAction = new VotoMasAction(cancion);
        const newState: CancionState = ReducerCancion(prevState,action2);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].votos).toEqual(1);
    });
    it('should reduce vote down', ()=>{
        let cancion = new Cancion('ee','ee','ee');          
        const action1: NuevaCancionAction = new NuevaCancionAction(cancion);
        const prevState: CancionState = ReducerCancion(initializaCancionState(),action1);
        const action2: VotoMenosAction = new VotoMenosAction(cancion);
        const newState: CancionState = ReducerCancion(prevState,action2);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].votos).toEqual(-1);
    });
});