import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { AppState } from './app.module';
import { Store } from '@ngrx/store';
import { VotoPositivoAction, VotoNegativoAction } from './models/canciones-state.model';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef, private store: Store<AppState>) { 
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento))
  }
  track(evento: Event): void {
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    switch (elemTags[0]) {
      case 'cancion_voto_mas':{
        this.store.dispatch(new VotoPositivoAction());
        break;
      }
      case 'cancion_voto_menos':{
        this.store.dispatch(new VotoNegativoAction());
        break;
      }    
      default:
        break;
    }
    //console.log(`||||||||| track evento: "${elemTags}"`)
  }
}
