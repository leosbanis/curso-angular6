import { Component, OnInit } from '@angular/core';
import { DescargasApiClientService } from '../descargas-api-client.service';
import { DescargaInterrumpidaApi } from '../descargas-interrumpidas-api-client';

@Component({
  selector: 'app-descargas-interrumpidas',
  templateUrl: './descargas-interrumpidas.component.html',
  styleUrls: ['./descargas-interrumpidas.component.css'],
  providers: [DescargaInterrumpidaApi,{provide:DescargasApiClientService, useExisting:DescargaInterrumpidaApi}]
})
export class DescargasInterrumpidasComponent implements OnInit {
  descargaInterrumpida;
  constructor(private descargaApiClient: DescargasApiClientService) {
    this.descargaInterrumpida = this.descargaApiClient.getById("id");
  }

  interrumpir(id: string){
    this.descargaInterrumpida = this.descargaApiClient.getById("id");
  }

  ngOnInit() {
   
  }
}
