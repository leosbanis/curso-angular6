import { Component, OnInit } from '@angular/core';
import { DescargasApiClientService } from '../descargas-api-client.service';
import { Descarga } from 'src/app/models/descarga.models';

@Component({
  selector: 'app-descargas-listado',
  templateUrl: './descargas-listado.component.html',
  styleUrls: ['./descargas-listado.component.css']
})
export class DescargasListadoComponent implements OnInit {
  listaDescargas: Descarga[];
  constructor(private api: DescargasApiClientService) { }

  ngOnInit() {
  }


}
