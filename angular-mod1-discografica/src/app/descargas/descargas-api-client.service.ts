import { Injectable } from '@angular/core';
import { Descarga } from '../models/descarga.models';

@Injectable({
  providedIn: 'root'
})
export class DescargasApiClientService {
  descargas: Descarga[];

  constructor() { this.descargas = [];}

  getAll(){
    return [{id:1, name:'uno'},{id:2, name:'dos'}];
  }
  add(idCancion: string){
    this.descargas.push(new Descarga(idCancion));
  }
  getById(id:string){
    return null;
  }
    
}
